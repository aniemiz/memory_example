//Interfaccia della classe MEMORY

#ifndef MEMORY_HPP
#define MEMORY_HPP
//Servono per definire la classe una volta sola

#include <systemc.h>

//Definisce il ritardo della memoria in numero di cicli di clock
#define DEL_MEM 100
//Definizione dell'errore di indirizzo non valido
#define MEM_ERR_INV_ADDRESS   1
//Dimensione della memoria allocata in parole di WRD_SIZE bit
#define MEM_SIZE   512
//Dimensione dell'indirizzo in bit
#define ADR_SIZE	10
//Numero di bit delle parole dei dati
#define WRD_SIZE	16
//Se la riga seguete non è commentata, il programma stampa a video informazioni di debug
//durante il funzionamento
#define PRINT_WHILE_RUN

using namespace sc_dt;

//Classe memory, implementa un banco di memoria di dimensione MEM_SIZE di parole di WRD_SIZE bit
SC_MODULE(Memory) 
{
public:
// Tipi di variabili enumerate, accessibili dall'esterno (public)
    //Tipo Function: indica alla memoria la prossima funzione da eseguire
    enum Function 
    {
        FUNC_NONE,
        FUNC_READ,
        FUNC_WRITE
    };

	//Tipo RETurnSignal: indica all'esterno lo stato della memoria / la funzione eseguita
	enum RETSignal 
	{
        RSIG_NONE,
        RSIG_READ_FIN,
        RSIG_WRITE_FIN,
        RSIG_ERROR
   };

//Porte del modulo
   sc_in<bool>       Port_CLK;
   sc_in<Function>   Port_Func;

   sc_in<sc_uint<ADR_SIZE> >	Port_Addr;			//Porta specializzata ingresso di tipo intero senza segno di ADR_SIZE bit
   sc_inout_rv<WRD_SIZE>			Port_Data;			//Porta specializzata inout di LOGIC VECTOR	di WRD_SIZE bit
   sc_out<RETSignal> 					Port_DoneSig;

//*****Variabile globale
//*****	sc_lv<WRD_SIZE> HImp explicit sc_lv('z'); //= sc_lv('z'); //Variabile per settare la porta dati in alta impedenza

//COSTRUTTORE
    SC_CTOR(Memory) 
    {
        //Metodo execute: processo sensibile al fronte negativo del clock
        SC_METHOD(execute);  
        sensitive << Port_CLK.neg();   
        
        //******HImp = explicit sc_lv('z');

        //Inizializzazione delle variabili private del modulo
        m_clkCnt  = 0;
        m_curAddr = 0;
        m_curData = 0;
        m_curFunc = Memory::FUNC_NONE; //NB: assegnazione a un valore del tipo enumerato

        //Implementazione del banco di memoria nella memoria centrale
        m_data = new sc_bv<WRD_SIZE>[MEM_SIZE];

        /* Initialize Counters */
        m_writesCnt = 0;
        m_readsCnt  = 0;
        m_errorsCnt = 0;
        m_errorCode = 0;
    }

//DISTRUTTORE: disalloca la memoria centrale
    ~Memory() {
        delete [] m_data;
    }

//Funzioni metodo del modulo per accedere alle variabili dall'esterno, intestazione ed implementazione

    int getErrorCode() 
    {
        return m_errorCode;
    }

    int getWritesCount() 
    {
        return m_writesCnt;
    }

    int getReadsCount() 
    {
        return m_readsCnt;
    }

    int getErrorsCount() 
    {
        return m_errorsCnt;
    }

    int getRequestsCount() 
    {
        return m_readsCnt  +
               m_writesCnt + m_errorsCnt;
    }

//Variabili del modulo, private
private:

    int								m_clkCnt;
    sc_uint<ADR_SIZE>	m_curAddr;
    sc_bv<WRD_SIZE>		m_curData;
    Function  				m_curFunc;


    sc_bv<WRD_SIZE>* 	m_data;
    int  m_errorCode;

    int  m_writesCnt;
    int  m_readsCnt;
    int  m_errorsCnt;

//Prototipi delle altre funzioni del metodo

	//Verifica se l'indirizzo è valido
	void checkAddress();
	
	//Verifica se il dato da scrivere è valido
	void checkData();
	
	//Funzione di lettura nella memoria
	RETSignal read();
	
	//Funzione di scrittura nella memoria
	RETSignal write();

	//Processo principale del modulo, sincrono con il clock
	void execute();
	
};
#endif
