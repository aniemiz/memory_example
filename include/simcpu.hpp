//Interfaccia della classe SIMCPU
//Tale classe simula il comportamento di una cpu nell'utilizzo della memoria
//serve per testare il funzionamento della classe MEMORY

#ifndef SIMCPU_HPP
#define SIMCPU_HPP

#include <systemc.h>
#include "memory.hpp"

//Se non commentata, la riga seguente induce a generare indirizzi non validi
#define GEN_INVALID_ADDRESSES

using namespace sc_dt;


SC_MODULE(SimCpu) 
{
		
	public:
	
		//Porte
    sc_in<bool>               	Port_CLK;
    sc_in<Memory::RETSignal>  	Port_MemDone;
    sc_out<Memory::Function>  	Port_MemFunc;
    sc_out<sc_uint<ADR_SIZE> > 	Port_MemAddr;		//Porta specializzata uscita di tipo intero senza segno di ADR_SIZE bit
    sc_inout_rv<WRD_SIZE>     	Port_MemData;		//Porta specializzata inout di LOGIC VECTOR	di WRD_SIZE bit

    // COSTRUTTORE
    SC_CTOR(SimCpu) 
    {
    	SC_METHOD(execCycle);
    		sensitive << Port_CLK.pos();
        	dont_initialize();

      	SC_METHOD(memDone);
        	sensitive << Port_MemDone;
        	dont_initialize();
        	
        SC_METHOD(OutUpdate);
        	sensitive << Port_CLK.pos();

      m_waitMem = false;
      addr = 0;
      f = Memory::FUNC_NONE;
      Data = "zzzzzzzzzzzzzzzz";
      
    }

	private:
		//Variabili
    bool m_waitMem;
    sc_uint<ADR_SIZE> addr;
    Memory::Function f;
    sc_lv<WRD_SIZE> Data;

		//Prototipi delle Funzioni
		
    //Generates a random memory request.
    Memory::Function getrndfunc();
    
    //Generates a random memory address.
		sc_uint<ADR_SIZE> getRndAddress();  
	
    //Generates some random data to store to or load from memory.
    sc_bv<WRD_SIZE> getRndData();
    
    //Processo di generazione dati per la memoria
		void execCycle();

    //Process that is executed when the memory finishes handling a request.
    void memDone();
    
    //Aggiornamento delle porte in uscita
    void OutUpdate();
};


#endif
