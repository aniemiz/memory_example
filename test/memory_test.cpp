//File che utilizza le classi Memory e SimCpu per testarne il funzionamento 

#include <systemc.h>
#include <signal.h>
#include "memory.hpp"
#include "simcpu.hpp"

/*????????????????????????????
 *
 * SIGINT and SIGTERM
 * signals' handler
 *

void sig_handler(int sigNum)
{
    sc_stop();
}

?????????????????????????????*/



int sc_main(int argc, char* argv[])
{
    /*????????????????????????????????????????
     *
     * Register SIGINT and SIGTERM
     * signal handlers 
     *

    signal(SIGINT,  sig_handler);
    signal(SIGTERM, sig_handler);

    ?????????????????????????????????????????*/

    cout << "\n\nCreating Modules............";

    /* Instantiate Modules */
    Memory mem("main_memory");
    SimCpu cpu("cpu");

    /* Signals */
    sc_signal<Memory::Function>  	sigMemFunc;
    sc_signal<Memory::RETSignal> 	sigMemDone;
    sc_signal<sc_uint<ADR_SIZE> > sigMemAddr;
    sc_signal_rv<WRD_SIZE>       	sigMemData;

    sc_clock clk;

    cout << "DONE\nConnecting Modules' Ports...";

    /* Connecting module ports with signals */
    mem.Port_Func(sigMemFunc);
    mem.Port_Addr(sigMemAddr);
    mem.Port_Data(sigMemData);
    mem.Port_DoneSig(sigMemDone);

    cpu.Port_MemFunc(sigMemFunc);
    cpu.Port_MemAddr(sigMemAddr);
    cpu.Port_MemData(sigMemData);
    cpu.Port_MemDone(sigMemDone);

    mem.Port_CLK(clk);
    cpu.Port_CLK(clk);

    cout << "DONE\n" << endl;

    /*-----------------------------------*/

#if defined(PRINT_WHILE_RUN)
    cout << "\nWhile running press CTRL+C to exit.\n" 
         << "Press Enter to start...";

    getchar();
    cout << endl << endl;
#else
    cout << "\nPress Enter to start...";
    getchar();
    cout << "\n\nRunning (press CTRL+C to exit)... ";
#endif

    
   // fflush(stdout); //?????????????


    /* Start Simulation */
    sc_start();

    /* Show Results */

    cout << "\n   Simulation Time       : " << sc_time_stamp()  << endl
         << "   Delta Cycles Executed : "   << sc_delta_count() << endl << endl;

    cout << "   Total Memory Requests : " << mem.getRequestsCount() << endl
         << "   Memory READs          : " << mem.getReadsCount()    << endl
         << "   Memory WRITEs         : " << mem.getWritesCount()   << endl
         << "   Invalid Accesses      : " << mem.getErrorsCount()   << endl;

    cout << "\nPress Enter to exit...";
    getchar();
    cout << endl << endl;

    return 0;
}
