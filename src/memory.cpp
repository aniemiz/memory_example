//Implementazione delle funzioni del modulo memory

#include <systemc.h>
#include "memory.hpp"

using namespace std;
using namespace sc_dt;

//Verifica se l'indirizzo è valido
void Memory::checkAddress() 
{
	if(m_curAddr >= MEM_SIZE)
		m_errorCode = MEM_ERR_INV_ADDRESS;
	else
		m_errorCode = 0;
}

//Verifica se il dato da scrivere è valido
void Memory::checkData()
{
	sc_logic DataRed;
	
	m_errorCode = 0;
	DataRed = Port_Data.read().and_reduce();
	if (DataRed == 'z' | DataRed == 'x')
	{
		m_errorCode = 1;
	}
	
	DataRed = Port_Data.read().or_reduce();
	if (DataRed == 'z' | DataRed == 'x')
	{
		m_errorCode = 1;
	}
}

//Funzione di lettura nella memoria
Memory::RETSignal Memory::read() 
{
	checkAddress();

	if(m_errorCode == 0) 
	{
		Port_Data.write(m_data[m_curAddr]);  //Necessaria conversione tra sc_bv e sc_lv, automatica?

		m_readsCnt++;
		return RSIG_READ_FIN;
	} 
	else 
	{
		m_errorsCnt++;
	return RSIG_ERROR;
	}
}

//Funzione di scrittura nella memoria
Memory::RETSignal Memory::write() 
{
	checkAddress();

	if(m_errorCode == 0) 
	{
		m_data[m_curAddr] = m_curData;
            
		m_writesCnt++;
		return RSIG_WRITE_FIN;
	} 
	else 
	{
		m_errorsCnt++;
		return RSIG_ERROR;
	}
}


//Processo principale del modulo, sincrono con il clock
void Memory::execute() 
{
	RETSignal retSig;
	
	/*Se c'è un compito da eseguire in coda, aspetta 100 colpi di clock, poi esegue la lettura/scrittura
	segnala sulla porta il codice risultato dell'operazione, porta lo stato della memoria in idle (FUNC_NONE)*/
	if(m_curFunc != Memory::FUNC_NONE) 
	{
		m_clkCnt++;
		if(m_clkCnt == DEL_MEM) 
		{
			retSig = Memory::RSIG_ERROR;

			switch(m_curFunc) 
			{
				case Memory::FUNC_READ  : { retSig = read();  break; }
				case Memory::FUNC_WRITE : { retSig = write(); break; }
			}

			Port_DoneSig.write(retSig);

//Serve per scrivere a video messaggi per il debug
#if defined(PRINT_WHILE_RUN)
                cout << "Memory : Finished " 
                     << (m_curFunc == Memory::FUNC_WRITE ? "write" : "read")
                     << " request. Addr = " <<  m_curAddr
                     << "  Time = " << sc_time_stamp() << endl;
#endif

			m_clkCnt  = 0;
			m_curFunc = Memory::FUNC_NONE;
		}
		return;
	}

	/*Se sulla porta trova che non sono richieste operazioni, termina il processo*/
	if(Port_Func.read() == Memory::FUNC_NONE) 
	{
		//Imposta la porta dati in alta impedenza
		Port_Data.write('Z');											//<==== controllare se scrive un bit solo o tutti a Z
		return;
	}

	// Read request's data and address
	m_curFunc = Port_Func.read();
	m_curAddr = Port_Addr.read();
	
	//Stampa a video le info di debug
#if defined(PRINT_WHILE_RUN)
        cout << "Memory : Received " 
             << (m_curFunc == Memory::FUNC_WRITE ? "write" : "read")
             << " request. Addr = " <<  m_curAddr
             << "  Time = " << sc_time_stamp() << endl;
#endif
	
	if(m_curFunc == Memory::FUNC_WRITE)
	{
		//Controlla se il dato e valido
		checkData();
		if(m_errorCode == 0)
		{
			//Se il dato è valido legge la porta
			m_curData = Port_Data.read(); 			//Necessaria conversione tra sc_lv e sc_bv, automatica?
			retSig = Memory::RSIG_NONE;
		}
		else
		{
			retSig = Memory::RSIG_ERROR;
			m_curFunc = Memory::FUNC_NONE;
			//Stampa a video le info di debug
#if defined(PRINT_WHILE_RUN)
      cout << "Error : Received Bad data"             
           << "  Time = " << sc_time_stamp() << endl;
#endif			
		}
	}
	else
	{
		retSig = Memory::RSIG_NONE;
	}

	// Scrive sulla porta il segnale di ritorno
	Port_DoneSig.write(retSig);
	
	
}



