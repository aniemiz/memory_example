//Implementazione delle funzioni del modulo SimCpu

#include <systemc.h>
#include "memory.hpp"
#include "simcpu.hpp"

using namespace std;
using namespace sc_dt;


//Generates a random memory request.
Memory::Function SimCpu::getrndfunc() 
	{
	int rndnum=(rand() % 10);
	if(rndnum < 5)
		return Memory::FUNC_READ;
	else
		return Memory::FUNC_WRITE;
	}

//Generates a random memory address.
	sc_uint<ADR_SIZE> SimCpu::getRndAddress() 
	{
#ifndef GEN_INVALID_ADDRESSES
		return (rand() % MEM_SIZE);
#else
		return (rand() % (MEM_SIZE + 100));
#endif
    }

//Generates some random data to store to or load from memory.
	sc_bv<WRD_SIZE> SimCpu::getRndData() 
	{
		return rand();				//Necessaria conversine di tipo, automatica?
	}

//Processo di generazione dati per la memoria
		void SimCpu::execCycle() 
		{
			//Se è in attesa del risultato della memoria, termina il processo
			if(m_waitMem) 
			{
				return;
			}

			//Select random a function, address and data
			addr = getRndAddress();
			f = getrndfunc();

			//Send some data if function is WRITE 
			if(f == Memory::FUNC_WRITE)
			{
				Data = getRndData();					
			}
			else
			{
				Data = "zzzzzzzzzzzzzzzz";
			}

      m_waitMem = true;
			
			//Stampa a video informazioni di debug
#if defined(PRINT_WHILE_RUN)
        cout << "CPU : Sent " 
             << (f == Memory::FUNC_WRITE ? "write" : "read")
             << " request. Addr = " <<  addr
             << "  Time = " << sc_time_stamp() << endl;
#endif
    }
         
//Process that is executed when the memory finishes handling a request.
    void SimCpu::memDone() 
    {
			//Se la memoria fornisce il codice di idle, termina il processo
			if(Port_MemDone.read() == Memory::RSIG_NONE) 
			{
         return;
      }        
      
		
			//Stampa a video informazioni di debug	
#if defined(PRINT_WHILE_RUN)
        cout << "CPU : Received "
       			 << (Port_MemDone.read() == Memory::RSIG_ERROR ? "ERROR_SIG" : "FIN_SIG")				
       			 << " from memory."
             << "  Time = " << sc_time_stamp() << endl;
        //cout << "Data receved: " << Port_MemData.read() << endl << endl;
#endif
			
			//Resetta la variabile di attesa risultato memoria
			m_waitMem = false;

			// Reset memory's function
			f = Memory::FUNC_NONE;
			
    }    
    
//Aggiornamento delle porte in uscita
		void SimCpu::OutUpdate()
		{
			Port_MemFunc.write(f);
			Port_MemAddr.write(addr);
			Port_MemData.write(Data);	
		}
		
		
